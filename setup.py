"""
    setup.py for SSSS
    install - installs ssss to ~/.local/lib/pythonX.Y/site-packages/ssss
        also installs the main script to ~/.local/bin
"""

from setuptools import setup
from setuptools import setup, find_packages

setup(
    name="ssss",
    version="1.1",
    description="Simple Static Site Synthesizer - SSSS",
    author="Brian Davis",
    author_email="intrepidhero@gmail.com",
    packages=find_packages(),
    scripts=["ssss"],
)
