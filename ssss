#!/usr/bin/python3

import os
import shutil
import getpass
import sys
import argparse
import markdown
from markdown.extensions.codehilite import CodeHiliteExtension
from markdown.extensions.admonition import AdmonitionExtension
from markdown.extensions.tables import TableExtension
import yaml
import glob
from datetime import datetime
import collections
import http.server
import socketserver
import subprocess
import jinja2


class SSSS:
    """ This class provides access to the site synthesizer """

    def __init__(self):

        arg_parser = argparse.ArgumentParser(
            description="Simple Static Site Synthesizer - SSSS...", prog="ssss"
        )
        arg_parser.add_argument(
            "-f", action="store_true", default=False, help="don't ask for confirmation"
        )
        arg_parser.add_argument("-v", action="store_true", default=False)
        arg_parser.add_argument(
            "command", choices=["gen", "pub", "test"], help="action to take"
        )
        arg_parser.add_argument(
            "sitedef", type=str, help="the filename of the sitedef.py"
        )

        args = arg_parser.parse_args()
        self.verbose = True
        self.force = args.f

        sitedef = open(args.sitedef).read()
        self.sitefolder = os.path.split(os.path.abspath(args.sitedef))[0]
        os.chdir(self.sitefolder)
        self.buildfolder = os.path.join(self.sitefolder, "dist")

        scope = {
            "os": os,
            "datetime": datetime,
        }

        exec(compile(sitedef, "sitedef.py", "exec"), scope)

        self.template_cache = {}
        self.global_context = {}  # the global context for rendering templates
        self.by_file_context = collections.defaultdict(dict)  # per file context

        if args.command == "gen":
            self.make_dist()
            scope["gen"](self)
        elif args.command == "pub":
            scope["pub"](self)
        elif args.command == "test":
            scope["test"](self)

    def set_context(self, key, value, filekey=None):
        if filekey is None:
            self.global_context[key] = value
        else:
            self.by_file_context[filekey][key] = value

    def make_dist(self):
        """ Create the dist folder to hold the generated site """
        if os.path.exists(self.buildfolder):
            if not self.force:
                result = (
                    input("Remove existing folder " + self.buildfolder + "(y/n):")
                    .lower()
                    .strip()
                )
            if not result.startswith("y"):
                sys.exit()
        shutil.rmtree(self.buildfolder)
        os.mkdir(self.buildfolder)

    def mkdir(self, folder):
        """ Create a subfolder in the generated site tree """
        # TODO: support more levels
        os.mkdir(os.path.join(self.buildfolder, folder))

    def copy_static(self, filename, relpath=None):
        """ Copy the static file [filename] to the dist folder. If relpath is
        define copy the file into a subfolder.
        """
        if relpath:
            dest = os.path.join(self.buildfolder, relpath, filename)
        else:
            dest = os.path.join(self.buildfolder, filename)
        shutil.copy(filename, dest)

    def load_tpl(self, filename):
        """ Load the required template. Returns a jinja2.Template object. """
        if not filename in self.template_cache:
            self.template_cache[filename] = jinja2.Template(open(filename, "r").read())
        return self.template_cache[filename]

    def load_md(self, filecontent):
        """ split markdown and the header """
        lines = filecontent.splitlines()
        raw_meta = ""
        if len(lines) > 0 and lines[0].startswith("---"):
            end_meta = lines[1:].index("---")
            raw_meta = "\n".join(lines[0:end_meta])
            content = "\n".join(lines[end_meta + 2 :])
        else:
            content = filecontent
        return content, raw_meta

    def parse_meta(self, raw_meta):
        """ parse out the YAML metadata header """
        meta = yaml.load(raw_meta)
        if meta is None:
            meta = {}
        if "tags" in meta:
            assert type(meta["tags"]) is list
        return meta

    def render_md(self, md):
        """ render the given markdown to HTML """
        html = markdown.markdown(
            md,
            extensions=[CodeHiliteExtension(), AdmonitionExtension(), TableExtension()],
        )
        if html is None:
            html = ""
        return html

    def render_tpl(self, template, context):
        """ render a jinja2.Template object given the context """
        if type(template) is str:
            template = self.load_tpl(template)
        return template.render(context)

    def write_out(self, filecontent, fn):
        """ Writes a file """
        fn = os.path.join(self.buildfolder, fn)
        of = open(fn, "w")
        print("Writing out: ", fn)
        of.write(filecontent)
        of.close()

    def load_render(self, tpl_key, mdfn):
        tpl = self.load_tpl(tpl_key)
        if not "content" in self.by_file_context[mdfn]:
            self.load_files(mdfn)
        cntx = self.by_file_context[mdfn]
        cntx.update(self.global_context)
        return self.render_tpl(tpl, cntx)

    def load_render_write(self, tpl_key, mdfn):
        """ Loads an md file (and template if not cached), renders the template
        with the stored context and writes out to the computed filename. """
        tpl = self.load_tpl(tpl_key)
        if not "content" in self.by_file_context[mdfn]:
            self.load_files(mdfn)
        cntx = self.by_file_context[mdfn]
        cntx.update(self.global_context)
        result = self.render_tpl(tpl, cntx)
        self.write_out(result, cntx["outfn"])

    def list_static(self, path=None, verbose=False):
        """ list all the non-md files in path """
        if path is None:
            path = self.sitefolder
        if verbose:
            print("Searching: %s for static files" % path)
        result = []
        for fn in glob.glob(os.path.join(path, "*")):
            if fn.endswith(".md") or fn.endswith(".py") or fn.endswith(".pyc"):
                continue
            result.append(fn)
        return result

    def list_md(self, path=None, verbose=False):
        """ list all the md files in path """
        if path is None:
            path = self.sitefolder
        if verbose:
            print("Searching: " + path)
        return glob.glob(os.path.join(path, "*.md"))

    def load_files(self, filenames, verbose=False, relpath=""):
        """ loads md files in a dictionary (filenames are keys) containing
        content (rendered to HTML by render_md), any meta data read from the
        YAML header, and the output filename as outfn. """
        everything = {}
        if type(filenames) is str:
            filenames = [filenames]
        for fn in filenames:
            if verbose:
                print(fn)
            raw = open(fn).read()
            content, meta = self.load_md(raw)
            try:
                everything[fn] = self.parse_meta(meta)
            except Exception as e:
                print("Invalid meta-data in: " + fn)
                raise e
            if "date" in everything[fn] and type(everything[fn]["date"]) is str:
                try:
                    everything[fn]["date"] = datetime.strptime(
                        everything[fn]["date"], "%Y-%m-%d"
                    )
                except:
                    print("Invalid date field in: %s, use YYYY-MM-DD" % fn)
                    raise
            else:
                everything[fn]["date"] = datetime.utcfromtimestamp(
                    os.path.getmtime(fn)
                )

            everything[fn]["content"] = self.render_md(content)
            basefn = os.path.splitext(os.path.basename(fn))[0]
            everything[fn]["outfn"] = os.path.join(relpath, basefn + ".html")
            if not "name" in everything[fn]:
                everything[fn]["name"] = basefn
            self.by_file_context[fn] = everything[fn]
        return everything

    def latest(self, files):
        """ returns most recent entry in files """
        results = []
        for key, meta in self.by_file_context.items():
            if not key in files:
                continue
            if not "date" in meta:
                continue
            results.append((key, meta["date"]))
        return list(sorted(results, key=lambda x: x[1]))[0][0]

    def by_tag(self, files):
        """ Returns a dictionary of all entries sorted by tag """
        tag_dict = {}
        for key, meta in self.by_file_context.items():
            if not key in files:
                continue
            if not "tags" in meta:
                continue
            for tag in meta["tags"]:
                if not tag in tag_dict:
                    tag_dict[tag] = {
                        "name": tag,
                        "links": [{"href": "/" + meta["outfn"], "name": meta["name"]}],
                    }
                else:
                    tag_dict[tag]["links"].append(
                        {"href": "/" + meta["outfn"], "name": meta["name"]}
                    )
        return list(sorted(tag_dict.values(), key=lambda x: x["name"]))

    def by_monthyear(self, files):
        """ Return a dictionary of all entries sorted by Year-Month """
        sections = {}
        for key, meta in self.by_file_context.items():
            if not key in files:
                continue
            if not "date" in meta:
                continue
            monthyear = meta["date"].strftime("%Y-%m")
            if not monthyear in sections:
                sections[monthyear] = {
                    "name": monthyear,
                    "links": [{"href": meta["outfn"], "name": meta["name"]}],
                }
            else:
                sections[monthyear]["links"].append(
                    {"href": meta["outfn"], "name": meta["name"]}
                )
        return list(sorted(sections.values(), key=lambda x: x["name"], reverse=True))

    def rsync_publish(self, user=None, host=None, destination=None, source=None):
        """ rsync the generated site to the server. """
        if user is None:
            user = getpass.getuser()
        if source is None:
            source = self.buildfolder
        if host is None:
            raise Exception("sitedef.pub() must specify the remote host")
        if destination is None:
            destination = os.path.join("/", "home", user, "html")
        host = user + "@" + host + ":" + destination
        cmd = ["rsync", "-arv", "-e", "ssh", source + "/", host]
        print(cmd)
        result = subprocess.run(cmd)

    def test(self, source=None, port=8080):
        """ Serve the generated site (in folder source) on localhost:[port]
        """
        if source is None:
            source = self.buildfolder

        class Handler(http.server.SimpleHTTPRequestHandler):
            def __init__(self, *args, **kwargs):
                super().__init__(*args, directory=source, **kwargs)

        with socketserver.TCPServer(("", port), Handler) as httpd:
            print("serving at port", port)
            httpd.serve_forever()


if __name__ == "__main__":
    SSSS()
